package com.example.project;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String[] logins, passwords, links;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logins = getResources().getStringArray(R.array.Logins);
        passwords = getResources().getStringArray(R.array.Passwords);
        links = getResources().getStringArray(R.array.Links);
    }

    public void button(View view) {
        int log = -1, pas = -2;
        EditText loginEt = (EditText) findViewById(R.id.LoginInput);
        String login = loginEt.getText().toString();

        EditText passwordEt = (EditText) findViewById(R.id.PasswordInput);
        String password = passwordEt.getText().toString();

        for (int i = 0; i < logins.length; i++) {
           if (login.equals(logins[i])) {
                 log = i;
                break;
            }
        }

        for (int i = 0; i < passwords.length; i++) {
            if (password.equals(passwords[i])) {
                pas = i;
                break;
            }
        }

        if (log != pas) {
            Toast toast = Toast.makeText(getApplicationContext(), "Wrong login or password", Toast.LENGTH_SHORT);
            toast.show();
        }
        else if (log==pas)  {
            Toast toast = Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT);
            toast.show();
            openSecondActivity(log);
        }
    }

    public void openSecondActivity(int log) {
        Intent intent = new Intent(this, SecondActivity.class);
     intent.putExtra("Login", logins[log]);
     intent.putExtra("Links", links[log]);
        startActivity(intent);
    }
}